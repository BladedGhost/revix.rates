using Moq;
using NUnit.Framework;
using Revix.Rates.Core.Interface;
using Revix.Rates.Core.Services;
using Revix.Rates.Data.Services;
using Revix.Rates.Models.DTO;
using Revix.Rates.Models.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Revix.Rates.Tests
{
    public class RatesTest: TestBase
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestHttpCall()
        {

            RatesModel data = null;
            Assert.DoesNotThrowAsync(async () => data = await HttpServiceMock.Object.Get<RatesModel>("fiat/map"));

            Assert.NotNull(data, "Data object was not fetched correctly");
            Assert.NotNull(data.Data);
            Assert.NotNull(data.Status);
            Assert.Null(data.Status.Error_message, "Something went wrong fetching data from the API");
            Assert.AreEqual(data.Status.Error_code, 0);
            Assert.Greater(data.Data.Count, 0);
        }

        [Test]
        public void TestRatesServiceFetch()
        {
            RatesModel data = null;
            Assert.DoesNotThrowAsync(async () => data = await RatesServiceMock.Object.FetchRates());


            Assert.NotNull(data, "Data object was not fetched correctly");
            Assert.NotNull(data.Data);
            Assert.NotNull(data.Status);
            Assert.Null(data.Status.Error_message, "Something went wrong fetching data from the API");
            Assert.AreEqual(data.Status.Error_code, 0);
            Assert.Greater(data.Data.Count, 0);
        }

        [Test] // tests fetching of the data
        public void TestRatesServiceGet()
        {
            var ratesObject = RatesServiceMock.Object;
            var rates = ratesObject.GetStoredRates();

            Assert.NotNull(rates);
            Assert.Greater(rates.Count, 0);
        }

        [Test] // tests fetching of the data
        public void TestRatesServiceGetByID()
        {
            var ratesObject = RatesServiceMock.Object;
            var singleRate = ratesObject.GetRate(0);
            Assert.NotNull(singleRate);
            Assert.AreEqual(singleRate.CurrencyCode, "USD");
        }

        [TestCase("United States Dollar", "USD")] // tests fetching of the data for USD
        [TestCase("Australian Dollar", "AUD")] // tests fetching of the data for AUD
        public void TestRatesServiceGetByCurrency(string currency, string expected)
        {
            var ratesObject = RatesServiceMock.Object;
            var ratesResults = ratesObject.GetRate(currency);

            Assert.NotNull(ratesResults);
            Assert.Greater(ratesResults.Count(), 0);
            Assert.AreEqual(ratesResults.FirstOrDefault().CurrencyCode, expected);
        }

        [TestCase(2781, "USD")] // tests fetching of the data for USD
        [TestCase(2782, "AUD")] // tests fetching of the data for AUD
        public void TestRatesServiceGetByCoinID(int coinID, string expected)
        {
            var ratesObject = RatesServiceMock.Object;
            var ratesResults = ratesObject.GetRateByCoinId(coinID);

            Assert.NotNull(ratesResults);
            Assert.AreEqual(ratesResults.CurrencyCode, expected);
        }
    }
}