﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Serilog;
using Revix.Rates.Core.Interface;
using Revix.Rates.Data;
using Microsoft.EntityFrameworkCore;
using Revix.Rates.Models.DTO;
using NUnit.Framework;
using EntityFrameworkCoreMock;
using System.Linq;
using Revix.Rates.Data.Interfaces;
using Revix.Rates.Data.Services;
using Revix.Rates.Core.Services;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Revix.Rates.Tests
{
    public class TestBase
    {
        public ILogger Logger;
        public Mock<IHttpClientFactory> HttpClientFactoryMock;
        public Mock<RatesService> RatesServiceMock;
        public Mock<HttpService> HttpServiceMock;
        public Mock<Repository<RatesDTO>> RatesRepoMock;
        public Mock<UnitOfWork> UnitOfWorkMock;

        public Mock<RatesDBContext> RatesDbContextMock;

        Mock<DbSet<RatesDTO>> RatesMock;

        [SetUp]
        public void SetupBase()
        {
            HttpClientFactoryMock = new Mock<IHttpClientFactory>();
            var httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri("https://sandbox-api.coinmarketcap.com/v1/");
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("*/*"));
            httpClient.DefaultRequestHeaders.Add("X-CMC_PRO_API_KEY", "42a84c3f-5848-4775-be96-60969a7248e8");

            HttpClientFactoryMock.Setup(x => x.CreateClient("RestAPI")).Returns(httpClient);

            Logger = Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Console().CreateLogger();

            var contextOptions = new DbContextOptionsBuilder<RatesDBContext>().UseInMemoryDatabase("Rates");
            RatesDbContextMock = new DbContextMock<RatesDBContext>(contextOptions.Options);

            SetupContext();


            var dbContextObject = RatesDbContextMock.Object;

            RatesRepoMock = new Mock<Repository<RatesDTO>>();
            RatesRepoMock.Setup(x => x.Context).Returns(dbContextObject);
            UnitOfWorkMock = new Mock<UnitOfWork>();
            UnitOfWorkMock.Setup(x => x.Context).Returns(dbContextObject);
            HttpServiceMock = new Mock<HttpService>(HttpClientFactoryMock.Object, Logger);
            RatesServiceMock = new Mock<RatesService>(RatesRepoMock.Object, HttpServiceMock.Object, UnitOfWorkMock.Object, Logger);
            RatesServiceMock.Setup(x => x.RatesRepo).Returns(RatesRepoMock.Object);
            RatesServiceMock.Setup(x => x.UnitOfWork).Returns(UnitOfWorkMock.Object);
        }

        private void SetupContext()
        {
            SetupDbSets();

            RatesDbContextMock.Setup(x => x.Rates).Returns(RatesMock.Object);
            RatesDbContextMock.Setup(x => x.Set<RatesDTO>()).Returns(RatesMock.Object);
        }

        private void SetupDbSets()
        {
            RatesMock = new Mock<DbSet<RatesDTO>>();
            var ratesData = new List<RatesDTO> { 
                new RatesDTO { CoinMarketCapID = 2781, Currency = "United States Dollar", CurrencySign = "$", CurrencyCode = "USD" },
                new RatesDTO { CoinMarketCapID = 2782, Currency = "Australian Dollar", CurrencySign = "$", CurrencyCode = "AUD" },
            }.AsQueryable();
            RatesMock.As<IEnumerable<RatesDTO>>().Setup(d => d.GetEnumerator()).Returns(ratesData.GetEnumerator());
            RatesMock.As<IQueryable<RatesDTO>>().Setup(m => m.Provider).Returns(ratesData.Provider);
            RatesMock.As<IQueryable<RatesDTO>>().Setup(m => m.Expression).Returns(ratesData.Expression);
            RatesMock.As<IQueryable<RatesDTO>>().Setup(m => m.ElementType).Returns(ratesData.ElementType);
            RatesMock.As<IQueryable<RatesDTO>>().Setup(m => m.GetEnumerator()).Returns(ratesData.GetEnumerator());
        }

        [Test(Description = "Testing in memory DB Context integrtiy")]
        public async Task TestContext()
        {
            RatesMock.Object.Add(new RatesDTO());
            RatesMock.Verify(x => x.Add(It.IsAny<RatesDTO>()), Times.Once);

            RatesDbContextMock.Object.SaveChanges();
            RatesDbContextMock.Verify(x => x.SaveChanges(), Times.Once);
        }
    }
}
