﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revix.Rates.Models.Models
{

    public class RatesModel
    {
        public StatusModel Status { get; set; }
        public List<DatumModel> Data { get; set; }
    }
}