﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revix.Rates.Models.Models
{
    public class DatumModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sign { get; set; }
        public string Symbol { get; set; }
    }
}
