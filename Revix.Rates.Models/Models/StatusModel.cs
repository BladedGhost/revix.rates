﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Revix.Rates.Models.Models
{
    public class StatusModel
    {
        public DateTime Timestamp { get; set; }
        public int Error_code { get; set; }
        public object Error_message { get; set; }
        public int Elapsed { get; set; }
        public int Credit_count { get; set; }
        public object Notice { get; set; }
    }
}
