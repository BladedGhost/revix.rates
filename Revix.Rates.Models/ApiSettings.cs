﻿using System;

namespace Revix.Rates.Models
{
    public class ApiSettings
    {
        public string URL { get; set; }
        public string APIKey { get; set; }
    }
}
