﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Revix.Rates.Models.DTO
{
    [Table("Rates")]
    public class RatesDTO
    {
        [Key]
        public virtual int RatesID { get; set; }
        public virtual string Currency { get; set; }
        public virtual string CurrencyCode { get; set; }
        public virtual string CurrencySign { get; set; }
        public virtual int CoinMarketCapID { get; set; }
    }
}
