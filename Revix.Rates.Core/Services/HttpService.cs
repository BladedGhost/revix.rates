﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Revix.Rates.Core.Interface;
using Revix.Rates.Models;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Core.Services
{
    public class HttpService: IHttpService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;

        public HttpService(IHttpClientFactory httpClientFactory, ILogger logger)
        {
            _httpClient = httpClientFactory.CreateClient("RestAPI");
            _logger = logger;
        }
        /// <summary>
        /// Gets data from the specified endpoint URL, the base URL is set in the setup code.
        /// </summary>
        /// <typeparam name="T">The genetic type we expect the data to be once it is retrieved from the URL</typeparam>
        /// <param name="endpoint">The sub URL to send or fetch data, the base URL is in the setup code, only use the endpoint part i.e. fiat/map</param>
        /// <returns>Returns the data of the gentic type</returns>
        public async Task<T> Get<T>(string endpoint)
        {
            // Creating a basic get request message so we can fetch data from the Coin API
            HttpRequestMessage message = new HttpRequestMessage(HttpMethod.Get, endpoint);

            //Generic result set to a default value since we don't know what it is, we cannot set a value to it.
            T result = default;
            try
            {
                //Send the request message to the specified endpoint URL
                using var response = await _httpClient.SendAsync(message);
                // Make sure the request is a success, else we throw an exception
                response.EnsureSuccessStatusCode();

                //Get the data returned in the repsonse message
                string data = await response.Content.ReadAsStringAsync();
                _logger.Information($"Data fetched: {data}");
                //Convert the JSON data to the genetic type specified once this method has been called
                result = JsonConvert.DeserializeObject<T>(data, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.All });
            }
            catch(HttpRequestException ex)
            {
                //log and rethrow this exception
                _logger.Error(ex, "Http Get Exception");
                throw new HttpRequestException("Something happened while we tried to fetch the data you requested.", ex);
            }

            return result;
        }
    }
}
