﻿using Revix.Rates.Core.Interface;
using Revix.Rates.Data.Interfaces;
using Revix.Rates.Models.DTO;
using Revix.Rates.Models.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Revix.Rates.Core.Services
{
    /// <summary>
    /// Handles all rate related data, fetching from the API and storing and fetchinv in the database
    /// </summary>
    public class RatesService : IRatesService
    {
        public virtual IRepository<RatesDTO> RatesRepo { get; set; }
        public virtual IUnitOfWork UnitOfWork { get; set; }
        private readonly IHttpService _httpService;
        private readonly ILogger _logger;

        public RatesService(IRepository<RatesDTO> ratesRepo, IHttpService httpService, IUnitOfWork unitOfWork, ILogger logger)
        {
            RatesRepo = ratesRepo;
            _httpService = httpService;
            UnitOfWork = unitOfWork;
            _logger = logger;
        }

        /// <summary>
        /// Fetch rates data from the coin API and then stores them into the database, the data will then be returned
        /// </summary>
        /// <returns>The Rates data fetched from the API</returns>
        public async Task<RatesModel> FetchRates()
        {
            _logger.Information("Getting rates");
            RatesModel result = await _httpService.Get<RatesModel>("fiat/map");
            SaveRates(result);

            _logger.Information("Rates fetched successfully.");
            return result;
        }

        /// <summary>
        /// Fetches all of the data stored in the rates table
        /// </summary>
        /// <returns>Returns the data from the rates table into the RatesDTO model</returns>
        public List<RatesDTO> GetStoredRates()
        {
            return RatesRepo.GetAll().ToList();
        }

        /// <summary>
        /// Async Get rates data based on the ID provided <seealso cref="GetRate(int)"/>
        /// </summary>
        /// <param name="id">The ID of the record you want to fetch</param>
        /// <returns>The record matching the ID provided into a RatesDTO object</returns>
        public async Task<RatesDTO> GetRateAsync(int id)
        {
            _logger.Information($"Fetching rates from storage with ID: {id}");
            return await RatesRepo.GetByIdAsync(id);
        }

        /// <summary>
        /// Get rates data based on the ID provided <seealso cref="GetRateAsync(int)"/>
        /// </summary>
        /// <param name="id">The ID of the record you want to fetch</param>
        /// <returns>The record matching the ID provided into a RatesDTO object</returns>
        public RatesDTO GetRate(int id)
        {
            _logger.Information($"Fetching rates from storage with ID: {id}");
            return RatesRepo.Get(x => x.RatesID == id).FirstOrDefault();
        }

        /// <summary>
        /// Get rates data based on the string filter provided <seealso cref="GetRate(int)"/>
        /// </summary>
        /// <param name="filterString">The string value used to fetch rates data from the rates table, this will try to match against the currency name, currency symble and currency code</param>
        /// <returns>Returns a list of records matching the provided string</returns>
        public IEnumerable<RatesDTO> GetRate(string filterString)
        {
            filterString = filterString.ToUpper();
            _logger.Information($"Fetching rates from storage with String: {filterString}");
            return RatesRepo.Get(x => x.Currency.ToUpper().Contains(filterString) || x.CurrencyCode.ToUpper().Contains(filterString) || x.CurrencySign.ToUpper().Contains(filterString));
        }

        /// <summary>
        /// Async Get rates data based on the Coin ID provided <seealso cref="GetRateByCoinId(int)"/>
        /// </summary>
        /// <param name="id">The Coin ID of the record you want to fetch</param>
        /// <returns>The record matching the Coin ID provided into a RatesDTO object</returns>
        public async Task<RatesDTO> GetRateByCoinIdAsync(int id)
        {
            _logger.Information($"Fetching rates from storage with Coin Market ID: {id}");
            return await RatesRepo.FirstOrDefaultAsync(x => x.CoinMarketCapID == id);
        }

        /// <summary>
        /// Get rates data based on the Coin ID provided <seealso cref="GetRateByCoinIdAsync(int)"/>
        /// </summary>
        /// <param name="id">The Coin ID of the record you want to fetch</param>
        /// <returns>The record matching the Coin ID provided into a RatesDTO object</returns>
        public RatesDTO GetRateByCoinId(int id)
        {
            _logger.Information($"Fetching rates from storage with Coin Market ID: {id}");
            return RatesRepo.Get(x => x.CoinMarketCapID == id).FirstOrDefault();
        }

        /// <summary>
        /// Async Saves the rates provided in the RatesModel object <seealso cref="SaveRates(RatesModel)"/>
        /// </summary>
        /// <param name="model">The rates model object either fetched from the Coin API or manually created, the records in the data property will be stored</param>
        /// <returns>Returns a task back</returns>
        public async Task SaveRatesAsync(RatesModel model)
        {
            _logger.Information("Saving rates to database");

            try
            {
                List<RatesDTO> data = model.Data.Select(x => new RatesDTO
                {
                    Currency = x.Name,
                    CurrencyCode = x.Symbol,
                    CurrencySign = x.Sign,
                    CoinMarketCapID = x.Id
                }).ToList();

                var repoData = RatesRepo.GetAll().ToList();
                var newData = data.Where(x => !repoData.Any(a => a.CoinMarketCapID == x.CoinMarketCapID));

                _logger.Information($"Total rate records to be stored: {newData.Count()}");

                await RatesRepo.AddRangeAsync(newData);
                await UnitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong while storing the rates data");
                throw;
            }

            _logger.Information("Saved all rates successfully");
        }

        /// <summary>
        /// Saves the rates provided in the RatesModel object <seealso cref="SaveRatesAsync(RatesModel)"/>
        /// </summary>
        /// <param name="model">The rates model object either fetched from the Coin API or manually created, the records in the data property will be stored</param>
        /// <returns>Returns a task back</returns>
        public void SaveRates(RatesModel model)
        {
            _logger.Information("Saving rates to database");

            try
            {
                List<RatesDTO> data = model.Data.Select(x => new RatesDTO
                {
                    Currency = x.Name,
                    CurrencyCode = x.Symbol,
                    CurrencySign = x.Sign,
                    CoinMarketCapID = x.Id
                }).ToList();

                var repoData = RatesRepo.GetAll().ToList();
                var newData = data.Where(x => !repoData.Any(a => a.CoinMarketCapID == x.CoinMarketCapID));

                _logger.Information($"Total rate records to be stored: {newData.Count()}");

                RatesRepo.AddRange(newData);
                UnitOfWork.Save();
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong while storing the rates data");
                throw;
            }

            _logger.Information("Saved all rates successfully");
        }
    }
}
