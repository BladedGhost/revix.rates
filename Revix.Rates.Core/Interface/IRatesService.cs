﻿using Revix.Rates.Models.DTO;
using Revix.Rates.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Core.Interface
{
    public interface IRatesService
    {

        /// <summary>
        /// Fetch rates data from the coin API and then stores them into the database, the data will then be returned
        /// </summary>
        /// <returns>The Rates data fetched from the API</returns>
        Task<RatesModel> FetchRates();

        /// <summary>
        /// Fetches all of the data stored in the rates table
        /// </summary>
        /// <returns>Returns the data from the rates table into the RatesDTO model</returns>
        List<RatesDTO> GetStoredRates();

        /// <summary>
        /// Async Get rates data based on the ID provided <seealso cref="GetRate(int)"/>
        /// </summary>
        /// <param name="id">The ID of the record you want to fetch</param>
        /// <returns>The record matching the ID provided into a RatesDTO object</returns>
        IEnumerable<RatesDTO> GetRate(string filterString);

        /// <summary>
        /// Get rates data based on the ID provided <seealso cref="GetRateAsync(int)"/>
        /// </summary>
        /// <param name="id">The ID of the record you want to fetch</param>
        /// <returns>The record matching the ID provided into a RatesDTO object</returns>
        Task<RatesDTO> GetRateByCoinIdAsync(int id);

        /// <summary>
        /// Get rates data based on the string filter provided <seealso cref="GetRate(int)"/>
        /// </summary>
        /// <param name="filterString">The string value used to fetch rates data from the rates table, this will try to match against the currency name, currency symble and currency code</param>
        /// <returns>Returns a list of records matching the provided string</returns>
        void SaveRates(RatesModel model);

        /// <summary>
        /// Async Get rates data based on the Coin ID provided <seealso cref="GetRateByCoinId(int)"/>
        /// </summary>
        /// <param name="id">The Coin ID of the record you want to fetch</param>
        /// <returns>The record matching the Coin ID provided into a RatesDTO object</returns>
        Task SaveRatesAsync(RatesModel model);

        /// <summary>
        /// Get rates data based on the Coin ID provided <seealso cref="GetRateByCoinIdAsync(int)"/>
        /// </summary>
        /// <param name="id">The Coin ID of the record you want to fetch</param>
        /// <returns>The record matching the Coin ID provided into a RatesDTO object</returns>
        RatesDTO GetRate(int id);

        /// <summary>
        /// Async Saves the rates provided in the RatesModel object <seealso cref="SaveRates(RatesModel)"/>
        /// </summary>
        /// <param name="model">The rates model object either fetched from the Coin API or manually created, the records in the data property will be stored</param>
        /// <returns>Returns a task back</returns>
        Task<RatesDTO> GetRateAsync(int id);

        /// <summary>
        /// Saves the rates provided in the RatesModel object <seealso cref="SaveRatesAsync(RatesModel)"/>
        /// </summary>
        /// <param name="model">The rates model object either fetched from the Coin API or manually created, the records in the data property will be stored</param>
        /// <returns>Returns a task back</returns>
        RatesDTO GetRateByCoinId(int id);
    }
}
