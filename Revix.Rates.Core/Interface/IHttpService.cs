﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Core.Interface
{
    public interface IHttpService
    {
        /// <summary>
        /// Gets data from the specified endpoint URL, the base URL is set in the setup code.
        /// </summary>
        /// <typeparam name="T">The genetic type we expect the data to be once it is retrieved from the URL</typeparam>
        /// <param name="endpoint">The sub URL to send or fetch data, the base URL is in the setup code, only use the endpoint part i.e. fiat/map</param>
        /// <returns>Returns the data of the gentic type</returns>
        Task<T> Get<T>(string endpoint);
    }
}
