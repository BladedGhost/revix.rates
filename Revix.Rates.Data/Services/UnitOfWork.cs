﻿using Revix.Rates.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Data.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        public virtual RatesDBContext Context { get; set; }
        public UnitOfWork()
        {

        }
        public UnitOfWork(RatesDBContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Executes the save command on the context saving all data in the current transaction <seealso cref="SaveAsync"/>
        /// </summary>
        public void Save()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// Async Executes the save command on the context saving all data in the current transaction <seealso cref="Save"/>
        /// </summary>
        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }
    }
}