﻿using Microsoft.EntityFrameworkCore;
using Revix.Rates.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Data.Services
{
    /// <summary>
    /// Handles all of the transactions to and from the Rates database
    /// </summary>
    /// <typeparam name="T">The object type matching the table we want to work with</typeparam>
    public class Repository<T> : IRepository<T> where T : class
    {
        //everything happening in this class will stay in memory until the Unit of Work class's Save is called
        // the DbContext here is set to public virtual so we can moq it during testing
        public virtual RatesDBContext Context { get; set; }
        public Repository()
        {

        }

        // setup the context here, dependancy injection will inject the context here

        public Repository(RatesDBContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Async call to add data to the context
        /// </summary>
        /// <param name="entity">The object entity matching the table we want to work with to be inserted into the table</param>
        /// <returns>Returns an async Task</returns>
        public async Task AddAsync(T entity)
        {
            var set = Context.Set<T>();
            await set.AddAsync(entity);
        }
        /// <summary>
        /// Async Adds a range of records to the entity in the context <seealso cref="AddRange(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entities">The object entity matching the table we want to work with to be inserted into the table</param>
        /// <returns>Returns an async Task</returns>
        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await Context.Set<T>().AddRangeAsync(entities);
        }
        /// <summary>
        /// Async Adds a range of records to the entity in the context <seealso cref="AddRangeAsync(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entities">The object entity matching the table we want to work with to be inserted into the table</param>
        public void AddRange(IEnumerable<T> entities)
        {
            Context.Set<T>().AddRange(entities);
        }
        /// <summary>
        /// Checks if there are any records in the selected set <seealso cref="AnyAsync(Expression{Func{T, bool}})"/>
        /// </summary>
        /// <returns>Returns a boolean value indicating if there are any records in the set of data or not</returns>
        public async Task<bool> AnyAsync()
        {
            return await Context.Set<T>().AnyAsync();
        }
        /// <summary>
        /// Checks if there are any records in the selected set <seealso cref="AnyAsync"/>
        /// </summary>
        /// <param name="expression">The expression to use to compare data to</param>
        /// <returns>Returns a boolean value indicating if there are any records in the set of data that returns true based on the expression provided</returns>
        public async Task<bool> AnyAsync(Expression<Func<T, bool>> expression)
        {
            return await Context.Set<T>().AnyAsync(expression);
        }
        /// <summary>
        /// Returns the first record in the entity based on the expression provided
        /// </summary>
        /// <param name="expression">The expression to use to filter thtough the data</param>
        /// <returns>Returns the first record matching in the expression</returns>
        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(expression);
        }
        /// <summary>
        /// Get a range of records matching with the provided expression
        /// </summary>
        /// <param name="expression">The expression to use to filter through the context set</param>
        /// <returns>Returns all the data matching the provided expression</returns>
        public IEnumerable<T> Get(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>().Where(expression);
        }
        /// <summary>
        /// Fetches all records from the context set matching the class model
        /// </summary>
        /// <returns>All records in the table will be returned</returns>
        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>();
        }
        /// <summary>
        /// Fethces the first record that matches the provided id
        /// </summary>
        /// <param name="id">The primary key ID we are trying to match against</param>
        /// <returns>Returns the first record matching the ID</returns>
        public async Task<T> GetByIdAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }
        /// <summary>
        /// Deletes the provided record matching the entity set <seealso cref="RemoveRange(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entity">The entity we want to remove from the record set</param>
        public void Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
        }
        /// <summary>
        /// Deletes the provided record matching the entity set <seealso cref="Remove(T)"/>
        /// </summary>
        /// <param name="entities">The entity we want to remove from the record set</param>
        public void RemoveRange(IEnumerable<T> entities)
        {
            Context.RemoveRange(entities);
        }
        /// <summary>
        /// Updates the record in the table with the entity provided <seealso cref="Update(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entity">The entity data we want to update to the table matching the exact record</param>
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }
        /// <summary>
        /// Updates the record in the table with the entity provided <seealso cref="Update(T)"/>
        /// </summary>
        /// <param name="entities">The entity data we want to update to the table matching the exact record</param>
        public void Update(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Context.Entry(entity).State = EntityState.Modified;
            }
        }
    }
}