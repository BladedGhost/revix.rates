﻿using Microsoft.EntityFrameworkCore;
using Revix.Rates.Models.DTO;
using System;

namespace Revix.Rates.Data
{
    public class RatesDBContext: DbContext
    {
        public RatesDBContext(DbContextOptions options):
            base (options)
        {

        }

        public virtual DbSet<RatesDTO> Rates { get; set; }
    }
}
