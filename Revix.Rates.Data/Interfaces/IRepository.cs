﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Data.Interfaces
{
    /// <summary>
    /// Handles all of the transactions to and from the Rates database
    /// </summary>
    /// <typeparam name="T">The object type matching the table we want to work with</typeparam>
    public interface IRepository<T> where T : class
    {

        /// <summary>
        /// Async call to add data to the context
        /// </summary>
        /// <param name="entity">The object entity matching the table we want to work with to be inserted into the table</param>
        /// <returns>Returns an async Task</returns>
        Task AddAsync(T entity);

        /// <summary>
        /// Async Adds a range of records to the entity in the context <seealso cref="AddRange(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entities">The object entity matching the table we want to work with to be inserted into the table</param>
        /// <returns>Returns an async Task</returns>
        void AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Async Adds a range of records to the entity in the context <seealso cref="AddRangeAsync(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entities">The object entity matching the table we want to work with to be inserted into the table</param>
        Task AddRangeAsync(IEnumerable<T> entities);

        /// <summary>
        /// Checks if there are any records in the selected set <seealso cref="AnyAsync(Expression{Func{T, bool}})"/>
        /// </summary>
        /// <returns>Returns a boolean value indicating if there are any records in the set of data or not</returns>
        Task<bool> AnyAsync();

        /// <summary>
        /// Checks if there are any records in the selected set <seealso cref="AnyAsync"/>
        /// </summary>
        /// <param name="expression">The expression to use to compare data to</param>
        /// <returns>Returns a boolean value indicating if there are any records in the set of data that returns true based on the expression provided</returns>
        Task<bool> AnyAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Returns the first record in the entity based on the expression provided
        /// </summary>
        /// <param name="expression">The expression to use to filter thtough the data</param>
        /// <returns>Returns the first record matching in the expression</returns>
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Get a range of records matching with the provided expression
        /// </summary>
        /// <param name="expression">The expression to use to filter through the context set</param>
        /// <returns>Returns all the data matching the provided expression</returns>
        IEnumerable<T> Get(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Fetches all records from the context set matching the class model
        /// </summary>
        /// <returns>All records in the table will be returned</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Fethces the first record that matches the provided id
        /// </summary>
        /// <param name="id">The primary key ID we are trying to match against</param>
        /// <returns>Returns the first record matching the ID</returns>
        Task<T> GetByIdAsync(int id);

        /// <summary>
        /// Deletes the provided record matching the entity set <seealso cref="RemoveRange(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entity">The entity we want to remove from the record set</param>
        void Remove(T entity);

        /// <summary>
        /// Deletes the provided record matching the entity set <seealso cref="Remove(T)"/>
        /// </summary>
        /// <param name="entities">The entity we want to remove from the record set</param>
        void RemoveRange(IEnumerable<T> entities);

        /// <summary>
        /// Updates the record in the table with the entity provided <seealso cref="Update(IEnumerable{T})"/>
        /// </summary>
        /// <param name="entity">The entity data we want to update to the table matching the exact record</param>
        void Update(T entity);

        /// <summary>
        /// Updates the record in the table with the entity provided <seealso cref="Update(T)"/>
        /// </summary>
        /// <param name="entities">The entity data we want to update to the table matching the exact record</param>
        void Update(IEnumerable<T> entities);
    }
}