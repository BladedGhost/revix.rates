﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Revix.Rates.Data.Interfaces
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Executes the save command on the context saving all data in the current transaction <seealso cref="SaveAsync"/>
        /// </summary>
        void Save();

        /// <summary>
        /// Async Executes the save command on the context saving all data in the current transaction <seealso cref="Save"/>
        /// </summary>
        Task SaveAsync();
    }
}