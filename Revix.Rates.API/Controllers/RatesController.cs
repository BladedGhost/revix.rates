﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Revix.Rates.Core.Interface;
using System.Net.Http;

namespace Revix.Rates.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RatesController : ControllerBase
    {
        private readonly ILogger _log;
        private readonly IRatesService _ratesService;

        public RatesController(ILogger log, IRatesService ratesService)
        {
            _log = log;
            _ratesService = ratesService;
        }

        [HttpGet("rates")]
        public async Task<IActionResult> Get()
        {
            IActionResult result = NotFound();
            try
            {
                result = Ok(_ratesService.FetchRates());
            }
            catch (HttpRequestException ex)
            {
                result = BadRequest(ex.Message);
            }
            return result;
        }

        [HttpGet("rates/stored")]
        public IActionResult GetStoredRates()
        {
            IActionResult result = NotFound();
            try
            {
                result = Ok(_ratesService.GetStoredRates());
            }
            catch (HttpRequestException ex)
            {
                result = BadRequest(ex.Message);
            }
            return result;
        }
    }
}
