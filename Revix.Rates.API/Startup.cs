using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Revix.Rates.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Revix.Rates.Core.Interface;
using Revix.Rates.Core.Services;
using Revix.Rates.Data.Interfaces;
using Revix.Rates.Data.Services;
using Revix.Rates.Models;

namespace Revix.Rates.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IWebHostEnvironment _env { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            services.AddDbContext<RatesDBContext>(x => x.UseSqlServer(Configuration.GetConnectionString("RatesConnection")));
            //setup swagger so we can execute the APIs using a simple GUI
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Revix.Rates.API", Version = "v1" });
            });
            // Setup serilog logger which will store to a text file under the Log folder in this app's root folder
            // Serilog can also log to a self hosted web page called Seq or to some other locations
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(@$"{_env.ContentRootPath}\Log\Rates.text", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            // seting up the dependancy injection for the logger
            services.AddSingleton<ILogger>(Log.Logger);
            // seting up the dependancy injection for the Configuration, this is incase we need some values from the appSettings file
            services.AddSingleton<IConfiguration>(Configuration);
            // seting up the dependancy injection for the HttpService
            services.AddScoped<IHttpService, HttpService>();
            // seting up the dependancy injection for the Repository which is dependant on the DbContext
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            // seting up the dependancy injection for the Uniot of Work which is dependant on the DbContext
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            // seting up the dependancy injection for the Rates service which will handle fetching and storing our rate data
            services.AddScoped<IRatesService, RatesService>();
            // setting up CORS so the browsers can fetch data from our API
            services.AddCors(x => x.AddPolicy("RatesPolicy", p =>
            {
                p.AllowAnyMethod();
                p.AllowAnyHeader();
                p.AllowAnyOrigin();
            }));
            // Fetching the api settings from the appsettings.json file so we can use it in the HttpClient
            ApiSettings apiSettings = Configuration.GetSection("ApiSettings").Get<ApiSettings>();
            // Sets up the Http Client object into an IHttpClientFactory which we can call through dependancy injection in the HttpClient, all the settings can be setup here so we do not need to do them over and over with every class that needs it
            services.AddHttpClient("RestAPI", x =>
            {
                x.BaseAddress = new Uri(apiSettings.URL);
                x.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("*/*"));
                x.DefaultRequestHeaders.Add("X-CMC_PRO_API_KEY", apiSettings.APIKey);
            });
        }

        // This method gets called by the runtime. This method is used to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            _env = env;
            app.UseCors("RatesPolicy");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Revix.Rates.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
